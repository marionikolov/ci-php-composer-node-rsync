# GitLab CI/CD Docker Image by Olive DC

This is a Docker image used for GitLab CI/CD at Olive Digital Consultancy. It is perfect for testing and deploying WordPress themes and plugins, which use Composer and Node.js tools and utilities.

The image is based on Debian/Ubuntu and has the following software installed:

* PHP (latest)
* Composer
* Node.js 20.9.0 LTS
* NPM
* rsync
* gettext
* Git
* OpenSSH

The following utilities have been installed to speed up Composer package installation:

* libzip-dev
* zip
* the php-zip extension

For reference, the image outputs software versions after it has been configured. This could be useful for debugging.

## Usage

Using this image in your CI/CD pipeline is as easy as referencing it in your `gitlab-ci.yml` file.

```
image: registry.gitlab.com/marionikolov/ci-php-composer-node-rsync/ci-php-composer-node-rsync

stages:
  - build
  - test
  - deploy

build:
  stage: build
  script:
    - composer install
    - npm install
    - npm run build

test:
  stage: test
  script:
    - npm run test

deploy:
  stage: deploy
  script:
    - rsync -avz ./public /app/public
```
