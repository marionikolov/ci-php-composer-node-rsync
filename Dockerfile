# Set component images
FROM composer:latest as COMPOSER

# Create base image
FROM php:latest

LABEL maintainer="Mario N. Nikolov <admin@olivedc.co.uk>"

# Copy artifacts from component images, i.e. install Composer
COPY --from=COMPOSER /usr/bin/composer /usr/bin/composer

# Update packages
RUN apt-get update

# Prepare Node.js installation
RUN apt-get install -y ca-certificates curl gnupg \
    && curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg \
    && NODE_MAJOR=20 \
    && echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list \
    && apt-get update

# Install required tools
RUN apt-get install -y --no-install-recommends \
    nodejs \
    gettext \
    # Deployment
	git \
	rsync \
    openssh-client \
    # Utilities
    unzip

# Echo software versions
RUN php -v \
    && composer -V \
    && echo Node.js "$(node -v)" \
    && echo NPM "$(npm -v)" \
    && rsync --version \
    && gettext -V \
    && git --version \
    && ssh -V

CMD ["bash"]
